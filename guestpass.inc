<?php

/**
 * @file
 * Defines a guestpass user.
 */

/**
 * Some constants
 */
define('GUEST_LINK_CRUMB', 'guest');

/**
 * Implements hook_help().
 *
 * @param $section
 *   string file path
 *
 * @return
 *   string
 */
function guestpass_help($page = 'admin/help#guestpass', $arg) {
  switch ($page) {
    case 'admin/help#guestpass':
      return t('<p>This module does stuff</p>');
    case 'admin/user/guestpass':
      return t('<p>Here be settings for Guestpass.</p>');
    case 'node/%node/guestpass':
      return t('<p> Creat a guestpass to share a link.</p>');
  }
}


/**
 * Implements hook_permission().
 *
 * @return
 *   array of permissions
 */

function guestpass_perm(){
    return array(
        'create a guestpass for own content'=>array(
            'title'=>t('Create a guestpass for own content'),
            'description'=>t('Create a guestpass for own content'),
        ),
        'delete a guestpass for own content' => array(
            'title' => t('Delete a guestpass for own content'),
            'description' => t('Delete a guestpass for own content'),
          ),
        'adminster guestpasses' => array(
            'title' => t('Adminster guestpasses'),
            'description' => t('Adminster guestpasses'),
            ),
    );
}


/**
 * Implements hook_menu().
 *
 * @return
 *   array of menu information
 */

function guestpass_menu(){
    global $user;
    $items = array();
    $items['guest/%']=array(
        'page callback'=>'guestpass_login_redirect_view',
        'page arguments'=>array(1),
        'type'=>MENU_CALLBACK,
        'access callback'=>TRUE,
    );
    $items['node/%node/guestpass']=array(
        'title'=>'guestpass',
        'page arguments'=>array('guestpass_create_guestpass_form',1),
        'page callback'=>'drupal_get_form',
        'access arguments'=>array('create a guestpass for own content',1),
        'access callback'=>'guestpass_create_access',
        'type'=> MENU_LOCAL_TASK,        
    );
    $items['admin/user/guestpass']=array(
        'title'=>'Guestpass settings',
        'page arguments'=>array('guestpass_admin_settings'),
        'page callback'=>'drupal_get_form',
        'access arguments' => array('administer site configuration'),
        'description' => 'Global configuration for guestpasses',
        'file' => 'guestpass.admin.inc',
        'type' => MENU_NORMAL_ITEM,        
    );
    return $items;
    
}


/**
 * Defines the access callback for the guestpass tab
 */
function guestpass_create_access($op, $node) {
  $guestpass_node_types = variable_get('guestpass_node_types','');
  if(!strcmp($guestpass_node_types[$node->type], $node->type)){
    global $user;
    if ($user->uid == $node->uid) {
      return TRUE;
    }
  }
  return FALSE;
}


/**
 * Page to create, view, and delete guestpasses
 */

function guestpass_create_guestpass_form($form, $form_state) {
    global $user, $base_root;
    //getting node id from url
    //$node_path = explode("/",$_GET['q']);
    //$nid = $node_path[1];
    $nid = arg(1);
    
    
    $header=array(
        'guest_link' => array('data' => t('Guestpass Link'), 'field' => 'g.guest_link'),
        'redirect_url' => array('data' => t('Page'), 'field' => 'g.redirect_url'),
    );
   
  $query = "SELECT * FROM {guestpass_relationships} WHERE host_uid = '%d'";
  $query = db_query($query,$user->uid);

  $options = array();
  while($result = db_fetch_object($query)) {
    /*
     
    $options[$result->guest_link] = array(
      'guest_link' => array(
            'data'=>array(
                '#type' => 'link',
                '#title' => check_plain($base_root . base_path() . $result->guest_link),
                '#href' => $base_root . base_path() . $result->guest_link,
             ),
        ),
      'redirect_url' => array(
            'data'=>array(
                '#type' => 'link',
                '#title' => check_plain($base_root . base_path() . $result->redirect_url),
                '#href' => $base_root . base_path() . $result->redirect_url,
             ),
        ),
    );
    
    */
    
    $options[$result->guest_link] = array(
      'guest_link' => check_plain($base_root . base_path() . $result->guest_link),
      'redirect_url' => check_plain($base_root . base_path() . $result->redirect_url),
    );
  }
  
  $form = array();
  $form['existing_guestpasses'] = array(
    '#type' => 'fieldset',
    '#title' => 'Guestpasses',
    '#weight' => 0,
  );
  $form['existing_guestpasses']['guestpasses'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('You currently do not have any guestpasses to share.'),
    '#multiple' => TRUE,
    '#weight' => 0,
    );
  $form['existing_guestpasses']['submit'] = array(
    '#type' => 'button',
    '#value' => t('Delete Selected Guestpass'),
    '#submit' => array('guestpass_delete_submit'),
    '#executes_submit_callback' => TRUE,
    '#weight' => 10,
  );
    
    
    $form['create_guestpass'] = array(
    '#type' => 'fieldset',
    '#title' => 'Create Guestpass',
    '#weight' => 1,
  );
    
  $form['create_guestpass']['guest_link'] = array(
    '#type' => 'value',
    '#value' => _guestpass_random_url(GUEST_LINK_CRUMB, 16),
  );
  $form['create_guestpass']['nid'] = array(
    '#type' => 'value',
    '#value' => $nid,
  );
  $form['create_guestpass']['host_uid'] = array(
    '#type' => 'value',
    '#value' => $user->uid,
  );
  
  if(variable_get('guestpass_email_settings','') == 0){
    $form['create_guestpass']['send_to_email'] = array(
      '#type' => 'textfield',
      '#title' => 'Recipient',
      '#description' => 'Email address of recipient.',
      //'#required' => TRUE,
    );
    $form['create_guestpass']['custom_message'] = array(
      '#type' => 'textarea',
      '#title' => 'Message',
      '#description' => 'Enter a personalized message.'
    );
  }
  
 
  
  $form['create_guestpass']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create guestpass'),
  );
  
    return $form;
}

/**
 * Callback for the delete button on the guestpass table.
 */

function guestpass_delete_submit($form,&$form_state){
    global $user,$base_root;
    $nid=$form_state['values']['nid'];
    $selected=$form_state['values']['guestpasses'];
    
     if(!empty($selected)){
        foreach($selected as $key){
            if($key != ''){
                drupal_set_message(check_plain('Deleted the guestpass ' . $base_root . base_path() . $key), 'alert');
                db_query("DELETE FROM {guestpass_relationships} WHERE guest_link ='%s'",$key);
            }
        }
    }else{
        drupal_set_message('There is no available guestpass to delete','warning');
    }
    
     
}

/**
 * Implements validate guestpass form
 */

/*
function guestpass_create_guestpass_form_validate($form, &$form_state) {
   if(variable_get('guestpass_email_settings','') == 0){
    $email=$form_state['values']['send_to_email'];
    if(empty($email)){
      form_set_error('', t('Please enter email to send guestpass'));
    }
    
     if (!_guestpass_check_email_address($email)) {
       form_set_error('', t('It appears you have entered an invalid email recipient.'));
    }
  }
 }
 
*/

/**
 *submit handler for create guestpass form
 */

function guestpass_create_guestpass_form_submit($form,&$form_state){
    global $base_root;
    $host_uid=$form_state['values']['host_uid'];
    $guestpass_link=$form_state['values']['guest_link'];
    $nid=$form_state['values']['nid'];
    $redirect_url='node/'.$nid;
    _guestpass_store_guest_link($host_uid, $guestpass_link, $redirect_url);
    drupal_set_message($base_root . base_path()  . $guestpass_link, 'guestpass', TRUE);
    $recipient = $form_state['values']['send_to_email'];
    $custom_message = $form_state['values']['custom_message'];
    if(!empty($recipient)){
      if(variable_get('guestpass_email_settings','') == 0 && $recipient != ''){
      guestpass_send_email($recipient, $custom_message, $guestpass_link);
      drupal_set_message('An email has been sent to '. $recipient, 'guestpass', TRUE);
      }
    }
    drupal_goto($redirect_url);
}



/**
 *Login and redirect the guest user
 */
function guestpass_login_redirect_view($guest_link_slug) {
  global $user,$base_root;
  $guest_link=GUEST_LINK_CRUMB . '/' . $guest_link_slug;
  if (!$user -> uid) {
    $new_guest = _guestpass_create_guest_user($guest_link);
    $form_state['uid'] = $new_guest -> uid;
    user_login_submit(array(), $form_state);
    drupal_set_message('You are viewing this site with a guestpass.', 'guestpass', TRUE);
  }
  // getting redirect url according to guestlink
  $query=db_query("SELECT * FROM {guestpass_relationships} WHERE guest_link = '%s'",$guest_link);
  while($result=db_fetch_object($query)){
      $redirect_url=$result->redirect_url;
  }
  // redirected to node page
  drupal_goto($redirect_url);
  
}


/**
 *store the guestlink in database
 */

function  _guestpass_store_guest_link($host_uid, $guestpass_link, $redirect_url){
    $link = $guestpass_link;
    $relationship = array(
    'host_uid' => $host_uid,
    'redirect_url' => $redirect_url,
    'guest_link' => $link,
  );
  drupal_write_record('guestpass_relationships', $relationship);
  module_invoke_all('create_guestpass', user_load($host_uid));
  return $link;
}



/**
 * Implements hook_cron().
 * Deletes guest users whose accounts are older than what is specified in the admin settings.
 *
 */

function guestpass_cron(){
  $cron_setting = variable_get('guestpass_time_autodelete', 0);
  $delta_time = variable_get('guestpass_delay', 3600);

  // If time to wait is set to never or cron is not enabled do nothing.
  if ($delta_time == 'never' || $cron_setting == 1) {
  }
  else {
    $query = db_query("SELECT * FROM {guestpass_guests} order by created");
    while($result=db_fetch_object($query)) {
      if ($result->created + $delta_time < time()) {
        //delete the user with guest role
        user_delete(array(),$result->guest_uid);
        //delete the guest user in guestpass table
        $query = db_query("DELETE FROM {guestpass_guests} WHERE guest_uid = '%d' ",$result->guest_uid);
        }
    }
  }
}




/**
 * Generate a random string
 */
function _guestpass_random_string($len = 16) {
  $length = $len;
  $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
  $string = '';
  for ($p = 0; $p < $length; $p++) {
      $string .= $characters[mt_rand(0, drupal_strlen($characters)-1)];
  }
  return $string;
}

/**
 * Generate a random URL
 */

function _guestpass_random_url($crumb, $len = 16) {
  $rand_string = _guestpass_random_string($len);
  $randURL = $crumb . '/' . $rand_string;
  return $randURL;
}

/**
 * Generate a random username
 */
function _guestpass_rand_username($prefix, $len = 16) {
  $rand_string = _guestpass_random_string($len);
  $randName = $prefix . '_' . $rand_string;
  return $randName;
}
/**
 * Generate dummy email address
 */
function _guestpass_rand_email($domain, $len = 16) {
  $rand_string = _guestpass_random_string($len);
  $randEmail = $rand_string . '@' . $domain;
  return $randEmail;
}


/**
 * Create guest user
 */
function _guestpass_create_guest_user($guest_link) {
  // create a new user by generating junk username ,email and password
  //getting user role by name
  $query=db_query("SELECT * FROM {role} WHERE name = '%s'",'guest');
  while($result=db_fetch_object($query)){
      $guest_role_id=$result->rid;
  }
  $guestaccount = array(
    'name' => _guestpass_rand_username(GUEST_LINK_CRUMB, 16),
    'pass' => user_password(10),
    'mail' => _guestpass_rand_email('nomail.local', 16),
    'status' => 1,
    'roles' => array($guest_role_id=> 'guest'),
    'init' => 'email address',
  );
  // alter the anonymous user object with the role of guest and save as a user
  $new_user = user_save(drupal_anonymous_user(), $guestaccount);
  // logged the user
  $new_user = user_authenticate($guestaccount);

  // put the user in the guestpass_guest table so we know s/he's a guest user
  $guest = array(
    'guest_link' => $guest_link,
    'guest_uid' => $new_user->uid,
    'created' => time(),
  );
  drupal_write_record('guestpass_guests', $guest);
  module_invoke_all('create_guestuser', $guest);
  return $new_user;
  
}

/**
 *validate the email
 */
function _guestpass_check_email_address($email) {
 // First, we check that there's one @ symbol, and that the lengths are right
        if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
            // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
            return false;
        }
        // Split it into sections to make life easier
        $email_array = explode("@", $email);
        $local_array = explode(".", $email_array[0]);
        for ($i = 0; $i < sizeof($local_array); $i++) {
            if (!preg_match("/^(([A-Za-z0-9!#$%&'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&'*+\/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
                return false;
            }
        }
        if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
            $domain_array = explode(".", $email_array[1]);
            if (sizeof($domain_array) < 2) {
                return false; // Not enough parts to domain
            }
            for ($i = 0; $i < sizeof($domain_array); $i++) {
                if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
                    return false;
                }
            }
        }

        return true;
    }


/**
 *implementation of hook_mail()
 */

function guestpass_mail($key, &$message, $params) {
  switch ($key) {
    case 'guestpass':
      $message['subject'] = $params['subject'];
      $message['body'] = $params['body'];      
      break;
  }
}

/**
 *send email to friends with guestpass
 */

function guestpass_send_email($recipient, $custom_message, $guestpass_link){
  global $user, $base_root;
    $module = 'guestpass';
    $key = '$user';
    $language = language_default();
    $params = array();
    $params['subject']=variable_get('message_title','');
    $default_message = variable_get('default_message','');
    $default_message = str_replace('<site_name>', variable_get('site_name', "Default site name"), $default_message);
    $default_message = str_replace('<guest_link>', $base_root . base_path() . $guestpass_link, $default_message);
    if(!empty($custom_message)){
    $params['body'][]=$custom_message;  
    }
    $params['body'][]=$default_message;
    $from = $user->mail;
    $send = TRUE;
    $message = drupal_mail($module, $key, $recipient, $language, $params, $from, $send);
    
}